<style>
    #formpendaftaran {
        height: 100vh;
    }
</style>
<section id="formpendaftaran">
    <div class="container mt-5">
        <div class="row justify-content-center">
          <div class="col-md-6">
            <div class="card">
              <div class="card-body p-5">
                <h5 class="card-title"><h2 class="text-center">Form Pendaftaran</h2></h5>
                <form>
                  <div class="form-group pt-3">
                    <label for="nama"><h5>Nama: </h5> </label>
                    <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama" required disabled>
                  </div>
                  <div class="form-group pt-3">
                    <label for="email"><h5>Email: </h5> </label>
                    <input type="email" class="form-control" id="email" placeholder="Masukkan Email" required disabled>
                  </div>
                  <div class="form-group pt-3">
                    <label for="tanggal"><h5>Tanggal Pelaksanaan:</h5></label>
                    <input type="date" class="form-control" id="tanggal" required>
                  </div>
                  <div class="form-group pt-3">
                    <label for="provinsi"><h5>Provinsi Sekolah:</h5></label>
                    <select class="form-control" id="provinsi" required>
                      <option value="">Pilih Provinsi</option>
                      <option value="Jawa">Jawa Barat</option>
                      <option value="Jawa">Jawa Barat</option>
                      <option value="Jawa">Jawa Barat</option>
                      
                    </select>
                  </div>
                  <div class="form-group pt-3">
                    <label for="provinsi"><h5>Asal Sekolah:</h5></label>
                    <select class="form-control" id="asal" required>
                      <option value="">Pilih Asal Sekolah</option>
                      <option value="SMA">SMA 1 ANONYMOUS</option>
                      <option value="SMA">SMA 1 ANONYMOUS</option>
                      <option value="SMA">SMA 1 ANONYMOUS</option>
                      
                    </select>
                  </div>
                  <div class="form-group pt-3">
                    <label for="anggota"><h5>Anggota:</h5></label>
                    <select class="form-control" name="anggota" id="anggota" required>
                        <option value="">Masukkan Nama Anggota Anda</option>
                        <option value="ANONIM">ANONIM</option>
                        <option value="ANONIM">ANONIM</option>
                        <option value="ANONIM">ANONIM</option>
                    </select>
                  </div>
                  <button type="submit" class="btn btn-dark d-block mx-auto px-5 mt-5">Kirim</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
</section>