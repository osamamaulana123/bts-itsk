<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <!-- Logo -->
        <a class="navbar-brand" href="#">
            <img src="img/logohitam.png" alt="Logo" height="120">
        </a>

        <!-- Toggle button untuk mobile -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Menu -->
        <div class="collapse navbar-collapse justify-content-between" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/#">Beranda</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/#manfaat">Manfaat</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/#faq">Daftar Pertanyaan</a>
                </li>
                @auth
                    <li class="nav-item">
                        <a class="nav-link" href="/#">Status</a>
                    </li>
                @endauth
            </ul>

            <!-- Tombol Sign Up atau Gambar Profile User -->
            @guest
                <form class="form-inline my-2 my-lg-0">
                    <button class="btn btn-dark px-5 my-2 my-sm-0" type="submit">Sign Up</button>
                </form>
            @else
                <!-- Ganti dengan gambar profile user -->
                <img src="#" alt="Profile Image" class="rounded-circle" height="30" width="30">
            @endguest
        </div>
    </div>
</nav>
