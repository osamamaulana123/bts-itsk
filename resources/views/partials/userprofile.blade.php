<div class=" card p-5 mb-5 mx-5">
    <h4 class="pb-3">Profil</h4>
    <div class="row">
        <!-- Kolom Pertama -->
        <div class="col-md-3">
            <div class="card mb-4">
                <img src="img/profile.png" class="rounded-circle mx-auto mt-4" alt="Profil Picture" style="width: 150px; height: 150px;">
                <div class="card-body text-center mt-4 mb-5 d-flex gap-5 px-3 justify-content-center">
                    <button class="btn btn-light border fw-bold"> <i data-feather="upload"></i> Pilih Foto</button>
                    <button class="btn btn-light border fw-bold"> <i data-feather="trash-2"></i> Hapus Foto</button>
                </div>
            </div>
        </div>

        <!-- Kolom Kedua -->
        <div class="col-md-6">
            <div class="card mb-4">
                <div class="card-header bg-white pt-3">
                    <h5>Informasi Profil</h5>
                </div>
                <div class="card-body">
                    <form>
                        <div class="mb-3">
                            <label for="nama" class="form-label"><h6>Nama</h6></label>
                            <input type="text" class="form-control" id="nama">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label"><h6>Email</h6></label>
                            <input type="email" class="form-control" id="email">
                        </div>
                    </form>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-header bg-white pt-3">
                    <h5>Pengaturan Login</h5>
                </div>
                <div class="card-body mb-3">
                    <form>
                        <div class="mb-3">
                            <label for="username" class="form-label"><h6>Username</h6></label>
                            <input type="text" class="form-control" id="username">
                        </div>
                        <label for="password" class="form-label"><h6>Password Saat Ini</h6></label>
                        <div class="input-group mb-3">
                            <input type="password" class="form-control " placeholder="Masukkan Password Anda"  aria-label="Recipient's username" aria-describedby="button-addon2" readonly>
                            <button class="btn btn-secondary" type="button" id="button-addon2"><i data-feather="eye-off"></i></button>
                        </div>
                        <label for="password" class="form-label"><h6>Password Baru</h6></label>
                        <div class="input-group mb-3">
                            <input type="password" class="form-control " placeholder="Tulis Password Baru Anda" aria-label="Recipient's username" aria-describedby="button-addon2" required>
                            <button class="btn btn-secondary" type="button" id="button-addon2"><i data-feather="eye-off"></i></button>
                        </div>
                        <div class="d-flex gap-2">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                <label class="form-check-label " for="flexCheckDefault"><p class="">huruf kecil</p></label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                <label class="form-check-label " for="flexCheckDefault"><p class="">huruf besar</p></label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                <label class="form-check-label " for="flexCheckDefault"><p class="">angka</p></label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                <label class="form-check-label " for="flexCheckDefault"><p class="">karakter/simbol</p></label>
                            </div>
                        </div>
                        <label for="password" class="form-label"><h6>Konfirmasi Password</h6></label>
                        <div class="input-group mb-3">
                            <input type="password" class="form-control " placeholder="Tulis Ulang Konfirmasi Password" aria-label="Recipient's username" aria-describedby="button-addon2" required>
                            <button class="btn btn-secondary" type="button" id="button-addon2"><i data-feather="eye-off"></i></button>
                        </div>
                        <button type="submit" class="btn btn-primary mt-3 px-5 float-end">Simpan</button>
                    </form>
                </div>
            </div>
        </div>

        <!-- Kolom Ketiga -->
        <div class="col-md-3">
            <div class="card mb-4">
                <div class="card-header bg-white pt-3">
                    <h5>Pemberitahuan</h5>
                </div>
                <div class="card-footer bg-white">
                    <button class="btn btn-primary mt-2" style="width: 100%"><i data-feather="bell"></i> Nyalakan</button>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-header bg-white pt-3">
                    <h5>Update</h5>
                </div>
                <div class="card-body">
                    <p class="text-danger text-center">Anda harus melakukan update profil anda sebelum melihat update"</p>
                </div>
                <div class="card-footer bg-white">
                    <button class="btn btn-primary mt-2" style="width: 100%">Update</button>
                </div>
            </div>
        </div>
    </div>
</div>
