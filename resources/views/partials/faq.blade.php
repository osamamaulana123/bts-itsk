<style>
    .list-group-item a {
        text-decoration: none; 
        color: #000;
    }
</style>

<section id="faq">
    <div class="container pb-5">
        <h1 class="pb-3 pt-3">Daftar Pertanyaan</h1>
        <ul class="list-group pb-5 mx-5">
            <li class="list-group-item"><a href="#">Bagaimana proses pendaftaran dan partisipasi dalam program ini?</a></li>
            <li class="list-group-item"><a href="#">Bagaimana alur kegiatan Back To School?</a></li>
            <li class="list-group-item"><a href="#">Apa saja kegiatan yang biasanya dilakukan dalam program Back To School?</a></li>
            <li class="list-group-item"><a href="#">Adakah informasi lebih lanjut tentang jadwal dan kegiatan yang akan diadakan selama pelaksanaan program ini?</a></li>
            <li class="list-group-item"><a href="#">Bagaimana proses pendaftaran dan partisipasi dalam program ini?</a></li>
    
            <!-- Tambahkan list-item sesuai dengan kebutuhan -->
        </ul>
    </div>
    
    <div class="container pb-5">
        <div class="text-center">
            <h2 class="pb-3">Ajukan Pertanyaan Anda</h2>
            <p class="pb-3">Jika informasi yang disampaikan kurang jelas anda bisa mengirim pertanyaan pada kolom berikut</p>
        </div>
        <form>
            <div class="input-group mb-3">
                <input type="text" class="form-control " placeholder="Masukkan pertanyaan anda" aria-label="Recipient's username" aria-describedby="button-addon2">
                <button class="btn btn-dark px-5" type="button" id="button-addon2">Kirim</button>
              </div>
        </form>
    </div>
</section>

