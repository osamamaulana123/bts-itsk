<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Kegiatan</title>
</head>
<body>
    <h1>Add Kegiatan</h1>
    @if ($errors->any())
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session('success'))
        <div>
            {{ session('success') }}
        </div>
    @endif
    <form action="{{ route('kegiatan.store') }}" method="POST">
        @csrf
        <div>
            <label for="ketua">Ketua:</label>
            <select name="id_user" id="id_user">
                @foreach ($usr as $user)
                    <option value="{{ $user->id }}">{{ $user->nama }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label for="anggota">Anggota:</label>
            <select name="id_user" id="id_user">
                @foreach ($usr as $user)
                    <option value="{{ $user->id }}">{{ $user->nama }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label for="provinsi">Provinsi:</label>
            <select name="id_provinsi" id="id_provinsi">
                @foreach ($prv as $provinsi)
                    <option value="{{ $provinsi->id }}">{{ $provinsi->provinsi }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label for="nama_sekolah">Nama Sekolah:</label>
            <input type="text" name="nama_sekolah" id="nama_sekolah">
        </div>
        <div>
            <label for="tanggal_kegiatan">Tanggal Kegiatan:</label>
            <input type="date" name="tanggal_kegiatan" id="tanggal_kegiatan">
        </div>
        <button type="submit">Submit</button>
    </form>
</body>
</html>
