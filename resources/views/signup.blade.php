@extends('layouts.loginform')

@section('title', 'BTS-ITSK | Sign Up')

@section('container')

    <div class="container p-5">
        <h1 class="fs-2 mb-5">Mulai Sekarang</h1>
        <form action="">
            <label for="nama" class="form-label fw-semibold mb-0">Nama</label>
            <input type="text" class="form-control mb-3" id="nama" placeholder="Masukkan nama" name="nama">

            <label for="email" class="form-label fw-semibold mb-0">Email</label>
            <input type="email" class="form-control mb-3" id="email" placeholder="Masukkan email" name="email">

            <label for="password" class="form-label fw-semibold mb-0">Password</label>
            <input type="password" class="form-control mb-3" id="password" placeholder="Masukkan password" name="password">

            <input class="form-check-input" type="checkbox" id="ingat" name="ingat">
            <label for="ingat" class="form-check-label fs-12px fw-semibold mb-3">Ingat selama 30 hari</label>

            <button type="submit" class="btn btn-dark full-width">Daftar</button>
        </form>
        <p class="text-center fs-12px fw-semibold mt-3 mb-3">Atau</p>
        <p class="text-center fs-12px fw-semibold mt-5">Sudah mempunyai akun? <a href="signin.blade.php" class="text-primary text-decoration-none">Masuk</a></p>
    </div>

@endsection