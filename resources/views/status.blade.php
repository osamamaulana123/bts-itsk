@extends('layouts.main')

@section('title', 'BTS-ITSK | Status')

@section('content')
    @include('partials.status')
@endsection
