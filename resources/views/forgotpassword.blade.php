@extends('layouts.loginform')

@section('title', 'BTS-ITSK | Forgot Password')

@section('container')

    <div class="container p-5">
        <h1 class="fs-2">Kesulitan Masuk?</h1>
        <p class="fs-14px fw-semibold mb-5">Masukkan email anda dan kami akan mengirimkan tautan untuk kembali ke akun anda!</p>
        <form action="">
            <label for="email" class="form-label fw-semibold mb-0">Email</label>
            <input type="email" class="form-control mb-5" id="email" placeholder="Masukkan email" name="email">

            <button type="submit" class="btn btn-dark full-width">Kirim</button>
        </form>
    </div>

@endsection