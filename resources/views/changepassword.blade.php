@extends('layouts.loginform')

@section('title', 'BTS-ITSK | Change Password')

@section('container')

    <div class="container p-5">
        <h1 class="fs-2">Buat Password Baru</h1>
        <p class="fs-14px fw-semibold mb-5">Buat password baru untuk login ke akun anda!</p>
        <form action="">
            <label for="password-baru" class="form-label fw-semibold mb-0">Password Baru</label>
            <input type="password" class="form-control mb-3" id="password-baru" placeholder="Masukkan password baru" name="password_baru">

            <label for="konfirmasi-password" class="form-label fw-semibold mb-0">Konfirmasi Password</label>
            <input type="password" class="form-control mb-5" id="konfirmasi-password" placeholder="Masukkan password baru" name="konfirmasi_password">

            <button type="submit" class="btn btn-dark full-width">Masuk</button>
        </form>
    </div>

@endsection