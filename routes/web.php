<?php

use App\Http\Controllers\KontenController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return view('index');
    } else {
        return view('welcome');
    }
});

Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

Route::get('/formpengajuan', function () {
    return view('formpengajuan');
});

Route::get('/profile', function () {
    return view('userprofile');
});

Route::prefix('/User')->group(function(){
    Route::get('/form', [KontenController::class, 'kegiatanUser_Add_View'])->name('form');
    Route::post('/store/kegiatan', [KontenController::class, 'KegiatanUser_Store'])->name('kegiatan.store');


});

Route::prefix('/admin')->group(function(){
    Route::get('/form', [KontenController::class, 'view_form_bahan_presentasi'])->name('form_file');
    Route::post('/store/file', [KontenController::class, 'store_bahan_presentasi'])->name('file.store');
});
