<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use App\Models\User;

class AuthController extends Controller
{
    function loginPage()
    {
        return view(""); // page login
    }

    function prosesLogin(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            if (Auth::user()->role == 'mahasiswa') { // untuk mahasiswa
                return redirect(''); // page user mahasiswa

            } elseif (Auth::user()->role == 'dosen_pembimbing') { // untuk dosen pembimbing
                return redirect(''); // page user dosen_pembimbing

            } else { // untuk admin
                return redirect(''); // page user admin
            }
        } else {
            return redirect('') // page login
                ->withErrors(["Login Gagal"])->withInput();
        }
    }

    function registerPage()
    {
        return view(""); // page register
    }

    public function processRegister(Request $request)
    {
        $request->validate([
            "nama"              => "required",
            "nim"               => "min:12|max:12|unique:users",
            "email"             => "required|unique:users",
            "password"          => "required|min:6",
        ]);

        $data = $request->all();

        $data['password'] = bcrypt($data['password']);
        $data['role'] = 'mahasiswa'; // Mahasiswa

        $user = User::create($data);

        event(new Registered($user));

        return redirect(""); // page setelah regis
    }

    public function logout() // need tombol logout di tampilan
    {
        Auth::logout();
        return redirect('views.signin');
    }
}
