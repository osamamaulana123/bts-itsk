<?php

namespace App\Http\Controllers;

use App\Models\bahan_presentasi;
use Illuminate\Http\Request;
use App\Models\kegiatan;
use App\Models\User;
use App\Models\provinsi;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
class KontenController extends Controller
{
    //view kegiatan
    // public function kegiatan_view()
    //     {
    //     $data['kgt'] = kegiatan::all();
    //     return view("view", $data);
    //     }
    public function status_view()
        {
        $data = kegiatan::with(['', '', ''])->get();
        return view('view', compact('data'));
        }
    //add kegiatan dari user
    function kegiatanUser_Add_View() {
       $usr = User::all();
       $prv = provinsi::all();
        return view ('Form',compact('usr','prv'));
    }

    public function KegiatanUser_Store(Request $request){
        $request->validate([
            'id_provinsi' => 'required|exists:provinsis,id',
            'id_user' => 'required|exists:users,id',
            'tanggal_kegiatan' => 'required|date',
        ]);

        $kegiatan = kegiatan::create([
            'id_provinsi' => $request->id_provinsi,
            'tanggal_kegiatan' => $request->tanggal_kegiatan,
            'status_promosi' => 'Pending',
        ]);

        $kegiatan->users()->attach($request->id_user);
    return redirect()->route('view')->with('success', 'Tambah Kegiatan berhasil');
}
    public function JadwalVIew()
        {
            $alldatajadwal = kegiatan::with(['', ''])->get();
            return view('user.jadwal', compact('alldatajadwal'));
        }


//Materi Kegiatan User
    public function view_form_bahan_presentasi() {
        return view('form_file');
    }
    public function store_bahan_presentasi(Request $request) {
        $request->validate([
            'nama_materi' => 'required|string',
            'status' => 'required|in:Aktif,NonAktif',
            'surat_tugas' => 'required|file|mimes:pdf,doc,docx',
            'files.*' => 'required|file|mimes:pdf,doc,docx,ppt'
        ]);
        // Upload surat tugas
        $srt = time() . '_' . $request->file('surat_tugas')->getClientOriginalName();
        $request->file('surat_tugas')->storeAs('public/files', $srt);

        $bahanPresentasi = bahan_presentasi::create([
            'nama_materi' => $request->nama_materi,
            'status' => $request->status,
            'surat_tugas' => $srt,
        ]);

        // Upload file materi
        foreach ($request->file('files') as $file) {
            $fileName = time() . '_' . $file->getClientOriginalName();
            $file->storeAs('public/files', $fileName);
            $bahanPresentasi->fileMateris()->create(['dokumen' => $fileName]);
        }

        return redirect()->route('view')->with('success', 'Bahan presentasi berhasil ditambahkan.');
    }
}
