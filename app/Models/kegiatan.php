<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kegiatan extends Model
{
    use HasFactory;
    protected $fillable = ['id_provinsi',
    'tanggal_kegiatan',
    'status_promosi',
    'catatan_promosi',
    'id_user',
    'id_kegiatan',
];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_kegiatans','id_kegiatan','id_user')->withTimestamps();
    }
    public function provinsi()
    {
        return $this->belongsTo(provinsi::class, 'id_provinsi')->withTimestamps();
    }

}
