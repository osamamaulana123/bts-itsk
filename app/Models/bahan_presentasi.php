<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class bahan_presentasi extends Model
{
    use HasFactory;

    protected $fillable = ['nama_materi', 'status', 'surat_tugas'];

    public function fileMateris()
    {
        return $this->belongsToMany(file_materi::class, 'bahan_presentasi_dan_file_materis', 'bahan_presentasi_id', 'file_materi_id')->withTimestamps();
    }
}
