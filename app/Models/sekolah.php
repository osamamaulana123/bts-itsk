<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sekolah extends Model
{
    use HasFactory;

    protected $fillable = ['nama_sekolah'];

    public function provinsi()
    {
        return $this->hasOne(provinsi::class, 'id_sekolah');
    }
}
