<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class laporan extends Model
{
    use HasFactory;

    public function file_laporan()
    {
        return $this->hasMany(file_laporan::class, 'id_file');
    }
}
